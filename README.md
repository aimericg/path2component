# path2component

## Getting started

Python script for converting rasterized component paths into active component paths all linked to 'comp'. To get started change the directory to your desired UFO file + '/glyphs'.

From then on all you have to do is execute your script.
