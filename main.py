import os
import xml.etree.ElementTree as ET

def calculate_middle_coordinate(points):
    x_coords = [int(point.attrib['x']) for point in points]
    y_coords = [int(point.attrib['y']) for point in points]
    middle_x = sum(x_coords) // len(points)
    middle_y = sum(y_coords) // len(points)
    return middle_x, middle_y

def replace_contours_with_components(element):
    for child in list(element):
        replace_contours_with_components(child)
        if child.tag == "contour":
            points = child.findall(".//point")
            middle_x, middle_y = calculate_middle_coordinate(points)
            
            # Create a new component element
            component_elem = ET.Element("component")
            component_elem.set("base", "comp")
            component_elem.set("xOffset", str(middle_x - 138))
            component_elem.set("yOffset", str(middle_y - 41))
            
            # Replace contour with component
            element.insert(list(element).index(child), component_elem)
            element.remove(child)

def process_glif_file(glif_file):
    tree = ET.parse(glif_file)
    root = tree.getroot()
    replace_contours_with_components(root)
    return tree

def main():
    glif_files_directory = "/Users/aimeric/Documents/xml-glif/10mal12Lampen.ufo/glyphs"
    output_directory = "/Users/aimeric/Documents/xml-glif/10mal12Lampen.ufo/glyphs"

    for filename in os.listdir(glif_files_directory):
        if filename.endswith(".glif"):
            input_filepath = os.path.join(glif_files_directory, filename)
            output_filepath = os.path.join(output_directory, filename)

            tree = process_glif_file(input_filepath)
            tree.write(output_filepath)

if __name__ == "__main__":
    main()
